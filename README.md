# Traefik Error Pages

These HTML pages are pulled into a Traefik service (dynamically) that's used for displaying **custom** errors pages such as 404s, etc.
